В первую очередь осуществил установку Astra Linux, всё было сделано по дефолту.

Диск был разбит следующим образом:

![Разбивка диска](https://gitlab.com/evgeny_v_v/create-box-astra-linux/raw/master/docs/img/Разбивка_диска.png)

Единственное что было сделано не по дефолту это настройка дополнительного ПО:

![выбор ПО](https://gitlab.com/evgeny_v_v/create-box-astra-linux/raw/master/docs/img/выбор_ПО.png)

Были убраны лишние программы и добавлены дополнительные средства удалённого доступа SSH.

После установки началась настройка. 

### Установка Guest Additions

Для того, чтобы Vagrant мог полнофункционально работать нужно установить Дополнения гостевой ОС. Смонтировал диск 

```
mount /dev/cdrom /media/cdrom
mount: block device /dev/sr0 is write-protected, mounting read-only
```

Помимо этого установил дополнительные пакеты командой:

apt-get install linux-headers-generic build-essential dkms --no-install-recommends

И, напоследок, запустил установку Guest Additions.

sh /media/cdrom/VBoxLinuxAdditions.run

### Беспарольный sudo

Для беспарольной работы необходимо настроить SSH.

Был добавлен новый пользователь "vagrant" который должен иметь беспарольный доступ к административным действиям. Это поведение можно изменить через Vagrantfile, но в данном случае, я просто добавим юзера vagrant, внес его в группу sudo, а для всех членов этой группы сделаем беспарольный доступ.

Беспарольный доступ делается через команду visudo, в ней находим следующую строчку и меняем на: 

```
# Allow members of group sudo to execute any command
%sudo  ALL=(ALL:ALL) NOPASSWD: ALL
```

Добавил authorized_keys для SSH доступа. Vagrant, по умолчанию, использует [этот приватный ключ](https://github.com/mitchellh/vagrant/tree/master/keys) для подключения. Значит нам нужно скопировать vagrant.pub и сохранить как authorized_keys.

```
mkdir /home/vagrant/.ssh
nano /home/vagrant/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant insecure public key
```

```
chmod 700 /home/vagrant/.ssh/
chmod 600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant:vagrant /home/vagrant/.ssh
```

Дополнительно прописали доступ для этих файлов.

На этом настройка Astra Linux завершена.