1)    Цель работы: изучения системных вызовов для работы с файлами и переменными окружения.

2)    Выполнение работы:

Заходим в созданный нами бокс.

Создаем новую папку для работы (mkdir work). Далее создаем файл “lab.c” с программой, которая добавляет в заранее созданный файл (lab.txt) значения аргументов командной строки и параметров окружающей среды.

Код программы:

```
#include <stdlib.h>                                                             
#include <stdio.h>                                                               int main (int argc, char* argv[], char* envp[])                                                              
{                                                                                                            
int j=1;                                                                                                     
FILE *new_file;                                                                 new_file=fopen("lab5.txt", "w+");                                               
printf ("adding %d arguments", argc);                                           
for (j=1; j<argc; j=j+1)                                                         
{                                                                               
fprintf (new_file, "args %d : %s\n", j, argv[j]);                               
}                                                                               
for (j=1; envp[j]!=0; j=j+1)                                                     
{fprintf (new_file, "env:%s\n", envp[j]);}                                       
printf ("%d var\n", j);                                                         
fclose(new_file);                                                               
}    
```

Далее создаем Makefile, в котором прописываем следующее:

```
lab5:lab5.o                       
        gcc lab5.o -o lab5 -lm                       
lab5.0:lab.c                      
        gcc -c lab.c              
install:                     
        mkdir -p bin          
        cp lab5 bin/lab5       
        rm -f lab5 lab5.o                         
uninstall:          
        rm -f bin/lab5                                                           
        rm -f bin/lab5.txt                                                       
        rmdir bin     
```

При запуске утилиты, с помощью команды “make” происходит компиляция программы “lab.c”. В цели install было прописано создание папки для хранения программы, её копирования туда и удаление из корня вспомогательных файлов. 

Последовательное выполнение работы представлено на рисунке 1.

![операция make](https://gitlab.com/evgeny_v_v/create-box-astra-linux/raw/master/docs/img/операция_make.png)

Результат работы программы в файле lab5.txt показан на рисунке 2.

![2.4](https://gitlab.com/evgeny_v_v/create-box-astra-linux/raw/master/docs/img/результат_лабараторной.png)

Рисунок 2 – Результат работы программы в файле lab.txt

 

3)    Вывод: вспомнил основы по работе с системными вызовами (open, read, write, close) и переменными окружения, а также укреплены знания по работе с утилитой “make”.