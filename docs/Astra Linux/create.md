# Создание Vagrant box-a

Виртуальная машина подготовлена. Теперь можно начать создание нового box-a. 

Предварительно с помощью mkdir vagrant создал папку в которой будем создавать бокс.

```
vagrant package --base 'astraL' --output astraL_template
```

После --base пишем название такое же как было написано в VirtualBox у созданной нами машины. 

После выполнения предыдущей команды, а это занимает не мало времени, надо терпеливо дождаться. Добавляем его в список доступных боксов.

```
vagrant box add astraL_template --name 'Astra'
```

Теперь создаем виртуальную машину на основе нашего box-а. Для этого редактируем Vagrantfile.

```
Vagrant.configure(2) do |config|
 config.vm.box = "ubuntu 14.04"
end
```

Осталось только запустить командой vagrant up

![vagrant up](https://gitlab.com/evgeny_v_v/create-box-astra-linux/raw/master/docs/img/vagrant_up.png)

В данном случае vagrant ругается на то что у нас стоит старая версия VirtualBox и она может быть не безопасна.

Дальше мы должны проверить правильно ли мы настроили вход по ключу. 

Для этого вводим команду vagrant ssh. С помощью неё мы заходим в наш запущенный бокс.

Вводим команду sudo -i и видим что получение root прав произошло без ввода пароля

![sudo](https://gitlab.com/evgeny_v_v/create-box-astra-linux/raw/master/docs/img/sudo.png)

На этом создание Vagrant Box завершено.